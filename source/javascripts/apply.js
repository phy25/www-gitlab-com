(function() {
  $('#select-department').on('change', function() {
    if (this.value === 'all') {
      $('h4.department-title, h5.office-title, div.job-container').removeClass('hidden');
      $('div.jobs-counter').addClass('hidden');
    } else {
      var dataSelection = ($('#select-department').find('option:selected').data('type') === 'department') ?
      '[data-department="' + this.value + '"]' : '[data-office="' + this.value + '"]';

      $('div.job-container, div.jobs-counter').removeClass('hidden');
      $('h4.department-title, h5.office-title').addClass('hidden');

      $('div.job-container').not(dataSelection).addClass('hidden');
      $('div.jobs-counter #counter').text($('div.job-container:visible').length + ' jobs');
    }
  });
})();
